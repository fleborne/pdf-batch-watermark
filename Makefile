SHELL := bash
.ONESHELL:
	.SHELLFLAGS := -eu -o pipefail -c
.DELETE_ON_ERROR:
	MAKEFLAGS += --warn-undefined-variables
	MAKEFLAGS += --no-builtin-rules
.SECONDARY:

# TEXT: The text to use for the watermark
TEXT ?= Text of the watermark

# DOCS_DIR: the root directory of the application documents
DOCS_DIR = source-dir

# ZIP_FILENAME: the filename of the final zip containing everything
ZIP_FILENAME ?= output.zip

# DIR: all the subdirectoris of DOCS_DIR to include in the application
DIR ?= subdir1 subdir2 subdir3

# BUILD_DIR: the destination directory for the build files
BUILD_DIR = build

# WATERMARKED_DIR: the destination directory for the watermarked documents
WATERMARKED_DIR = ${BUILD_DIR}/watermarked

# WATERMARK: the watermark file
WATERMARK = ${BUILD_DIR}/watermark.pdf

# OBJ_PATH: list all objects to watermark
OBJ_PATH = $(foreach dir, ${DIR}, $(wildcard ${DOCS_DIR}/${dir}/*.pdf))

# OBJ_WATERMARKED: list all watermarked objects
OBJ_WATERMARKED = $(addprefix ${WATERMARKED_DIR}/, $(subst ${DOCS_DIR}/,,${OBJ_PATH}))

.DEFAULT_GOAL := ${ZIP_FILENAME}

${WATERMARK}: watermark.tex
	[ -d ${BUILD_DIR} ] || mkdir ${BUILD_DIR}
	pdflatex \
		--output-directory=build \
		"\def\watermark{${TEXT}}\input watermark.tex"

${WATERMARKED_DIR}/%.pdf: ${DOCS_DIR}/%.pdf ${WATERMARK}
ifndef DIR
	$(error DIR is undefined)
endif
	-[ -d ${WATERMARKED_DIR} ] || mkdir ${WATERMARKED_DIR}
	-$(foreach \
		dir, \
		${DIR}, \
		[ -d ${WATERMARKED_DIR}/${dir} ] || mkdir ${WATERMARKED_DIR}/${dir};)
	pdftk "$<" multistamp ${WATERMARK} output $@

%.zip: ${OBJ_WATERMARKED} ${WATERMARK}
ifndef DIR
	$(error DIR is undefined)
endif
	zip -j $@ ${OBJ_WATERMARKED}

.PHONY: watermark
watermark: ${OBJ_WATERMARKED} ${WATERMARK}

.PHONY: clean
clean:
	-rm -rf ${WATERMARKED_DIR}
	-rm -rf ${BUILD_DIR}
	-rm ${WATERMARK}
	-rm ${ZIP_FILENAME}

.PHONY: test
test:
	$(foreach dir, ${DIR}, echo ${dir};)
	printf '%s\n' ${OBJ_PATH}
	printf '%s\n' ${OBJ_WATERMARKED}
