# Batch PDF watermark utility

This repo contains a simple LaTeX file and Makefile that **automate the process of adding a watermark** to every page of a list of PDF files.

All PDF files found reccursively in a base directory are processed and added to zip archive once watermarked.

## Dependencies

- **LaTeX** (see https://www.latex-project.org/get/ for installation instructions)
- **PDFtk Server**, available as a **pdftk** package on many Linux distributions (details here: https://www.pdflabs.com/tools/pdftk-server/)

## Configuration

### Watermark text

The text of the watermark is set in the `TEXT` variable of the *Makefile*. To change the size, font, placement, ... of the watermark, you should edit the *watermark.tex* file.

To change the langage used to display the date, change the value of the Babel package option in *watermark.tex*, line 3.

### Output

All build files (LaTeX build files and watermarked PDF) are stored in the `BUILD_DIR` directory.

All watermarked files are compressed into a zip archive named `ZIP_FILENAME`.

## Usage

### Files to watermark

The files must be organised as follows, with `DOCS_DIR` and `DIR` being defined in the *Makefile*.

```
pdf-batch-watermark
├── ${DOCS_DIR}
│   ├── ${DIR}[0]
│   │   ├── file1.pdf
│   │   └── file2.pdf
│   ├── ...
│   └── ${DIR}[N]
│       ├── file3.pdf
│       └── file4.pdf
├── Makefile
└── watermark.tex
```

### Running the code

Simply call `make` to generate a zip archive containing all the watermarked files.

To clean the build files, use `make clean`.

Finally, to troubleshoot errors, you can use `make test` to display the value of some of the variables automatically set in the *Makefile*.
